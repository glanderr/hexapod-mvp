#include <Servo.h> //Die Servobibliothek wird aufgerufen. Sie wird benötigt, damit die Ansteuerung des Servos vereinfacht wird.
#include <IRremote.hpp>


class Leg {
  Servo servoBody;
  int bodyServoOffset;
  Servo servoKnee;
  int kneeServoOffset;
  public:
  void attach(int servoPin0, int servoPin1, int bodyServoOffsetIn, int kneeServoOffsetIn){
    bodyServoOffset = bodyServoOffsetIn;
    kneeServoOffset = kneeServoOffsetIn;
    servoBody.write(90+bodyServoOffset);
    servoKnee.write(30+kneeServoOffset);
    servoBody.attach(servoPin0);
    servoKnee.attach(servoPin1);
  }
  void moveDeg(int bodyDeg,int kneeDeg){
    servoBody.write(bodyDeg+bodyServoOffset);
    servoKnee.write(kneeDeg+kneeServoOffset);
  }
};

class Body {
  public:
  int degMov = 30;
  Leg legFR;
  Leg legFL;
  Leg legMR;
  Leg legML;
  Leg legBR;
  Leg legBL;
  void moveDegAll(int bodyDeg,int kneeDeg){
    legFR.moveDeg(bodyDeg,kneeDeg);
    legFL.moveDeg(bodyDeg,kneeDeg);
    legMR.moveDeg(bodyDeg,kneeDeg);
    legML.moveDeg(bodyDeg,kneeDeg);
    legBR.moveDeg(bodyDeg,kneeDeg);
    legBL.moveDeg(bodyDeg,kneeDeg);
  }
  void attachLegFR(int servoPin0, int servoPin1, int bodyServoOffset, int kneeServoOffset){
    legFR.attach(servoPin0,servoPin1,bodyServoOffset,kneeServoOffset);
  }  
  void attachLegFL(int servoPin0, int servoPin1, int bodyServoOffset, int kneeServoOffset){
    legFL.attach(servoPin0,servoPin1,bodyServoOffset,kneeServoOffset);
  }
  void attachLegMR(int servoPin0, int servoPin1, int bodyServoOffset, int kneeServoOffset){
    legMR.attach(servoPin0,servoPin1,bodyServoOffset,kneeServoOffset);
  }
  void attachLegML(int servoPin0, int servoPin1, int bodyServoOffset, int kneeServoOffset){
    legML.attach(servoPin0,servoPin1,bodyServoOffset,kneeServoOffset);
  }
  void attachLegBR(int servoPin0, int servoPin1, int bodyServoOffset, int kneeServoOffset){
    legBR.attach(servoPin0,servoPin1,bodyServoOffset,kneeServoOffset);
  }
  void attachLegBL(int servoPin0, int servoPin1, int bodyServoOffset, int kneeServoOffset){
    legBL.attach(servoPin0,servoPin1,bodyServoOffset,kneeServoOffset);
  }
  void standUp(){
    moveDegAll(90,30);

  }  
  void sitDown(){
    moveDegAll(90,95);
  }
  void threeLeg1(){
    legFR.moveDeg(90, 30);
    legML.moveDeg(90, 120);
    legBR.moveDeg(90, 30);
    legFL.moveDeg(90, 30);
    legMR.moveDeg(90, 120);
    legBL.moveDeg(90, 30);
  }
  void test(){
    delay(1000);

    legFR.moveDeg(110, 30);
    legML.moveDeg(70, 30);
    legBR.moveDeg(110, 30);

    legFL.moveDeg(70, 90);
    legMR.moveDeg(110, 90);
    legBL.moveDeg(70, 90);

    delay(1000);

    legFR.moveDeg(110, 90);
    legML.moveDeg(70, 90);
    legBR.moveDeg(110, 90);


    legFL.moveDeg(70, 30);
    legMR.moveDeg(110, 30);
    legBL.moveDeg(70, 30);

  }
  void moveForward(){
    //standUp();
    int onGround = 25;
    int inAir = 75;
    int delay_sec = 150;
    int gourndDelaySec = delay_sec * 2;

    legFR.moveDeg(90, onGround);
    legML.moveDeg(90, onGround);
    legBR.moveDeg(90, onGround);

    legFL.moveDeg(90, inAir);
    legMR.moveDeg(90, inAir);
    legBL.moveDeg(90, inAir);


    delay(delay_sec);

    legFR.moveDeg(90-degMov, onGround);
    legML.moveDeg(90+degMov, onGround);
    legBR.moveDeg(90-degMov, onGround);

    legFL.moveDeg(90-degMov, onGround);
    legMR.moveDeg(90+degMov, onGround);
    legBL.moveDeg(90-degMov, onGround);  


    delay(gourndDelaySec);

    legFR.moveDeg(90, inAir);
    legML.moveDeg(90, inAir);
    legBR.moveDeg(90, inAir);

    legFL.moveDeg(90, onGround);
    legMR.moveDeg(90, onGround);
    legBL.moveDeg(90, onGround); 

    
    
    delay(delay_sec);

    legFR.moveDeg(90+degMov, onGround);
    legML.moveDeg(90-degMov, onGround);
    legBR.moveDeg(90+degMov, onGround);

    legFL.moveDeg(90+degMov, onGround);
    legMR.moveDeg(90-degMov, onGround);
    legBL.moveDeg(90+degMov, onGround); 


    delay(gourndDelaySec);
    /*delay(1000);

    legFR.moveDeg(90, 90);
    legML.moveDeg(90, 90);
    legBR.moveDeg(90, 90);*/
  }

    void moveBackward(){
    //standUp();
    int onGround = 25;
    int inAir = 75;
    int delay_sec = 150;
    int gourndDelaySec = delay_sec * 2;

    legFR.moveDeg(90, onGround);
    legML.moveDeg(90, onGround);
    legBR.moveDeg(90, onGround);

    legFL.moveDeg(90, inAir);
    legMR.moveDeg(90, inAir);
    legBL.moveDeg(90, inAir);


    delay(delay_sec);

    legFR.moveDeg(90+degMov, onGround);
    legML.moveDeg(90-degMov, onGround);
    legBR.moveDeg(90+degMov, onGround);

    legFL.moveDeg(90+degMov, onGround);
    legMR.moveDeg(90-degMov, onGround);
    legBL.moveDeg(90+degMov, onGround);  


    delay(gourndDelaySec);

    legFR.moveDeg(90, inAir);
    legML.moveDeg(90, inAir);
    legBR.moveDeg(90, inAir);

    legFL.moveDeg(90, onGround);
    legMR.moveDeg(90, onGround);
    legBL.moveDeg(90, onGround); 

    
    
    delay(delay_sec);

    legFR.moveDeg(90-degMov, onGround);
    legML.moveDeg(90+degMov, onGround);
    legBR.moveDeg(90-degMov, onGround);

    legFL.moveDeg(90-degMov, onGround);
    legMR.moveDeg(90+degMov, onGround);
    legBL.moveDeg(90-degMov, onGround); 


    delay(gourndDelaySec);
    /*delay(1000);

    legFR.moveDeg(90, 90);
    legML.moveDeg(90, 90);
    legBR.moveDeg(90, 90);*/
  }

  void turnRight(){
    int onGround = 25;
    int inAir = 75;
    int delay_sec = 150;
    int gourndDelaySec = delay_sec * 2;

    legFR.moveDeg(90, onGround);
    legML.moveDeg(90, onGround);
    legBR.moveDeg(90, onGround);

    legFL.moveDeg(90, inAir);
    legMR.moveDeg(90, inAir);
    legBL.moveDeg(90, inAir);


    delay(delay_sec);

    legFR.moveDeg(90+degMov, onGround);
    legML.moveDeg(90+degMov, onGround);
    legBR.moveDeg(90+degMov, onGround);

    legFL.moveDeg(90-degMov, onGround);
    legMR.moveDeg(90-degMov, onGround);
    legBL.moveDeg(90-degMov, onGround);  


    delay(gourndDelaySec);

    legFR.moveDeg(90, inAir);
    legML.moveDeg(90, inAir);
    legBR.moveDeg(90, inAir);

    legFL.moveDeg(90, onGround);
    legMR.moveDeg(90, onGround);
    legBL.moveDeg(90, onGround); 

    
    
    delay(delay_sec);

    legFR.moveDeg(90-degMov, onGround);
    legML.moveDeg(90-degMov, onGround);
    legBR.moveDeg(90-degMov, onGround);

    legFL.moveDeg(90+degMov, onGround);
    legMR.moveDeg(90+degMov, onGround);
    legBL.moveDeg(90+degMov, onGround); 


    delay(gourndDelaySec);
    }

  void turnLeft(){
    int onGround = 25;
    int inAir = 75;
    int delay_sec = 150;
    int gourndDelaySec = delay_sec * 2;

    legFR.moveDeg(90, onGround);
    legML.moveDeg(90, onGround);
    legBR.moveDeg(90, onGround);

    legFL.moveDeg(90, inAir);
    legMR.moveDeg(90, inAir);
    legBL.moveDeg(90, inAir);


    delay(delay_sec);

    legFR.moveDeg(90-degMov, onGround);
    legML.moveDeg(90-degMov, onGround);
    legBR.moveDeg(90-degMov, onGround);

    legFL.moveDeg(90+degMov, onGround);
    legMR.moveDeg(90+degMov, onGround);
    legBL.moveDeg(90+degMov, onGround);  


    delay(gourndDelaySec);

    legFR.moveDeg(90, inAir);
    legML.moveDeg(90, inAir);
    legBR.moveDeg(90, inAir);

    legFL.moveDeg(90, onGround);
    legMR.moveDeg(90, onGround);
    legBL.moveDeg(90, onGround); 

    
    
    delay(delay_sec);

    legFR.moveDeg(90+degMov, onGround);
    legML.moveDeg(90+degMov, onGround);
    legBR.moveDeg(90+degMov, onGround);

    legFL.moveDeg(90-degMov, onGround);
    legMR.moveDeg(90-degMov, onGround);
    legBL.moveDeg(90-degMov, onGround); 


    delay(gourndDelaySec);
  }
  void dance(){
    int onGround = 25;
    int inAir = 75;
    int delay_sec = 150;
    int gourndDelaySec = delay_sec * 2;

    legFR.moveDeg(150, 120);
    legFL.moveDeg(90, 120);

    legMR.moveDeg(120, onGround);
    legML.moveDeg(60, onGround);
    legBR.moveDeg(60, onGround);
    legBL.moveDeg(120, onGround);

    delay(gourndDelaySec);

    legFR.moveDeg(120, 180);
    legFL.moveDeg(60, 180);

    legMR.moveDeg(120, onGround);
    legML.moveDeg(60, onGround);
    legBR.moveDeg(60, onGround);
    legBL.moveDeg(120, onGround);


    delay(gourndDelaySec);


    legFR.moveDeg(90, 120);
    legFL.moveDeg(30, 120);

    legMR.moveDeg(120, onGround);
    legML.moveDeg(60, onGround);
    legBR.moveDeg(60, onGround);
    legBL.moveDeg(120, onGround);


    delay(gourndDelaySec);


    legFR.moveDeg(120, 180);
    legFL.moveDeg(60, 180);

    legMR.moveDeg(120, onGround);
    legML.moveDeg(60, onGround);
    legBR.moveDeg(60, onGround);
    legBL.moveDeg(120, onGround);


    delay(gourndDelaySec);

  }
  void praiseTheSun(){
    int onGround = 25;
    int inAir = 75;
    int delay_sec = 150;
    int gourndDelaySec = delay_sec * 2;

    legFR.moveDeg(90, 180);
    legFL.moveDeg(90, 180);

    legMR.moveDeg(120, onGround);
    legML.moveDeg(60, onGround);
    legBR.moveDeg(60, onGround);
    legBL.moveDeg(120, onGround);

    delay(delay_sec);

  }
  void transportPackaging(){
    int onGround = 25;
    int inAir = 75;
    int delay_sec = 150;
    int gourndDelaySec = delay_sec * 2;

    legFR.moveDeg(120, onGround);
    legML.moveDeg(0, 0);
    legBR.moveDeg(95, onGround);

    legFL.moveDeg(95, onGround);
    legMR.moveDeg(180, 0);
    legBL.moveDeg(120, onGround);

    delay(gourndDelaySec);
  }
};

//Servo servo1; //Erstellt für das Programm ein Servo mit dem Namen servo1
//Servo servo2;
Body body;

const int RECV_PIN = 11;
IRrecv irrecv(RECV_PIN);
decode_results results;
//Leg leg1(servo1,servo2);
/*Leg leg2(servo1,servo2);
Leg leg3(servo1,servo2);
Leg leg4(servo1,servo2);
Leg leg5(servo1,servo2);
Leg leg6(servo1,servo2);*/

int moveCounter = 0;
int loopCounter = 0;
void setup() {
// int servoBodyDefaultAngle = 90;
// int servoKneeDefaultAngle = 30;
  // put your setup code here, to run once:
//leg1.attach(A4,A5);
  body.attachLegFR(7,8,-17,16);
  //body.attachLegFR(3,2,-17,16);
  body.attachLegFL(A0,A1,26,6);
  body.attachLegMR(5,6,0,18);
  body.attachLegML(A2,A3,-15,5);
  body.attachLegBR(3,4,-30,10);
  body.attachLegBL(A4,A5,0,7);

//delay(1000);
  body.standUp();

  Serial.begin(9600);
  irrecv.enableIRIn();
  //delay(1000);
  //body.standUp();
  //body.legFR.moveDeg(90,90);
  //servo1.write(60);
  delay(1000);
  //body.test();
//body.moveDegAll(90,30);
//delay(150); //Das Programm stoppt für 3 Sekunden

}


void loop() {
  

  if (irrecv.decode(&results)){
        // note: the IR Codes for your remote controll can be tested with the following.
        Serial.println("HEX?");
        Serial.println(results.value, HEX);

        Serial.println("decimal?");
        Serial.println(results.value);
        irrecv.resume();

        // note: the IR Codes used in the if statements must be adapted to your remote control
        if(results.value == 6228 || results.value == 4180) {
          body.moveForward();
          body.standUp();
          Serial.println("move forward");

        }
        if(results.value == 4182 || results.value == 6230) {
          body.turnRight();
          body.standUp();
          Serial.println("turn right");

        }
        if(results.value == 4188 || results.value == 6229) {
          body.turnLeft();
          body.standUp();
          Serial.println("turn left");

        }
        if(results.value == 4179 || results.value == 6227) {
          body.moveBackward();
          body.standUp();
          Serial.println("move backward");

        }
        if(results.value == 4172 || results.value == 6220) {
          body.transportPackaging();

          Serial.println("Transform into Package position");

        }
        if(results.value == 4213 || results.value == 6261) {
          body.standUp();
          
          Serial.println("Stand Up");

        }
        if(results.value == 4206 || results.value == 6254) {
          body.dance();
          
          Serial.println("Dance Motherfucker");

        }
  }
}

